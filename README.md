# Tasks on Rack

This is a small app I will be using in a blog post for explaing HTTP from scratch.

This is also a playground for nix:
`cd` in, run `puma`. Done. Amazing.

## Gemfile, config.ru and bin

The usual. The content of `bin` is generated with `bundle binstubs puma`.

## shell.nix

The heart of this thing:

* Use nixpkgs 23.05
* Use ruby (but no packages, that's what we use bundler for, right?) and curl

## envrc

* Use the nix integration: This rocks, hard. Just cd into the folder, and you have your shell of choice plus all the things defined in shell.nix
* Add bin to the path for binstubs
* Set an environment variable, for funsies
