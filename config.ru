require "rack"
require "./app"

use Rack::Lint if ENV["RACK_ENV"] == "development"

use Rack::ContentType, "text/html; charset=utf-8"

# Following https://guides.rubyonrails.org/rails_on_rack.html#inspecting-middleware-stack

# use Rack::Sendfile
# use Rack::Static
use Rack::Runtime
use Rack::MethodOverride
use Rack::CommonLogger, $stderr
use Rack::ShowExceptions if ENV["RACK_ENV"] == "development"
use Rack::Reloader if ENV["RACK_ENV"] == "development"
# https://github.com/rack/rack-session
use Rack::Head
use Rack::ConditionalGet
use Rack::ETag
use Rack::TempfileReaper

run App.new
