# frozen_string_literal: true

require "tilt/erubi"

class App
  TASK_PATH = %r{\A/task/(\d+)\z}
  NOT_FOUND = "<!DOCTYPE html><html lang=\"en\"><title>Not found</title><h1>Not found</h1></html>"
  TASKS_TEMPLATE = Tilt::ErubiTemplate.new("tasks.html.erb")

  def initialize
    @tasks = []
  end

  def call(env)
    request = Rack::Request.new(env)

    if request.path == "/" && request.get?
      show_tasks
    elsif request.path == "/task" && request.post?
      create_task(request.params["task"])
    elsif TASK_PATH =~ request.path && request.delete?
      delete_task(Regexp.last_match(1).to_i)
    else
      not_found
    end
  end

  def show_tasks
    body = TASKS_TEMPLATE.render(nil, tasks: @tasks)
    respond_with(body)
  end

  def create_task(task)
    @tasks.push(task)
    redirect_to("/")
  end

  def delete_task(id)
    @tasks.delete_at(id)
    redirect_to("/")
  end

  def not_found
    respond_with(NOT_FOUND, 404)
  end

  def redirect_to(target)
    respond_with("", 302, { "Location" => target })
  end

  def respond_with(body, status = 200, headers = {})
    Rack::Response.new(body, status, headers).finish
  end
end
