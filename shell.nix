{ pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/nixos-23.05.tar.gz") {} }:

pkgs.mkShell {
  packages = [
    pkgs.ruby_3_2
    pkgs.curl
  ];
}
